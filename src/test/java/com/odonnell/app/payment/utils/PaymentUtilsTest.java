package com.odonnell.app.payment.utils;



import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.odonnell.app.payment.pojo.Payment;

class PaymentUtilsTest {

  PaymentUtils utils;

  @BeforeEach
  void setup() {
    utils = new PaymentUtils();

  }

  @Test
  void csvParserTest() throws IOException {
    File inputCsv = new File("src/test/resources/testData.csv");
    List<Payment> payments = utils.parsePersonsCsv(inputCsv);

    Payment testPayment1 = payments.get(0);
    Payment csvPayment1 = Payment.builder().creditorAccount("crdt-1").debtorAccount("dtr-1")
        .amount(new BigDecimal(100)).date(Date.valueOf("2018-09-26")).build();
    Payment csvPaymentBad = Payment.builder().creditorAccount("crdt-11").debtorAccount("dtr-1")
        .amount(new BigDecimal(100)).date(Date.valueOf("2018-09-26")).build();

    // comparing by toString value to avoid issues with comparing java.sql.date
    assertThat(testPayment1.toString(), equalTo(csvPayment1.toString()));
    assertThat(testPayment1.toString(), not((equalTo(csvPaymentBad.toString()))));


  }


  @Test
  void filterByMonthTest() {

    Date date = Date.valueOf("2018-09-26");
    int september = 9;
    int october = 10;

    assertThat(utils.filterByMonth(september, Date.valueOf("2018-09-26")), is(true));
    assertThat(utils.filterByMonth(october, Date.valueOf("2018-09-26")), is(false));

    assertThat(utils.filterByMonth(october, Date.valueOf("2018-10-26")), is(true));
    assertThat(utils.filterByMonth(september, Date.valueOf("2018-10-26")), is(false));


  }



}
