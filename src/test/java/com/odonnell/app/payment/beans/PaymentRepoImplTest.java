package com.odonnell.app.payment.beans;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import java.io.File;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.odonnell.app.payment.pojo.Payment;
import com.odonnell.app.payment.utils.PaymentUtils;

class PaymentRepoImplTest {

  PaymentRepoImpl implementation;

  @BeforeEach
  public void setup() {
    File inputCsv = new File("src/test/resources/testData.csv");
    PaymentUtils utils = new PaymentUtils();
    implementation = new PaymentRepoImpl(inputCsv, utils);
  }

  @Test
  void sumByCreditorAccountTest() {
    BigDecimal result = implementation.sumByCreditorAccount("crdt-1");
    assertThat(result, equalTo(new BigDecimal(318)));
  }

  @Test
  void containsTest() {
    Payment payment = Payment.builder().creditorAccount("crdt-1").debtorAccount("dtr-1")
        .amount(new BigDecimal(100)).date(Date.valueOf("2018-09-26")).build();
    Payment badPayment = Payment.builder().creditorAccount("crdt-111").debtorAccount("dtr-11")
        .amount(new BigDecimal(100)).date(Date.valueOf("2018-09-26")).build();

    assertThat(implementation.contains(payment), is(true));
    assertThat(implementation.contains(badPayment), is(false));
  }

  @Test
  public void filterByMonthTest() {
    BigDecimal september = new BigDecimal(510);
    BigDecimal october = new BigDecimal(645);

    assertThat(september, equalTo(implementation.sumByMonth(9)));
    assertThat(october, equalTo(implementation.sumByMonth(10)));
    assertThat(BigDecimal.ZERO, equalTo(implementation.sumByMonth(1)));
  }

  @Test
  public void uniqueDebtorListTest() {

    List<String> listUniqueDAccounts = implementation.listUniqueDebtorAccounts();
    Collections.sort(listUniqueDAccounts);
    Set<String> setUniqueDAccounts = listUniqueDAccounts.stream().collect(Collectors.toSet());

    assertThat(listUniqueDAccounts, hasSize(9));
    assertThat(setUniqueDAccounts.size(), equalTo(listUniqueDAccounts.size()));
    assertThat(setUniqueDAccounts.containsAll(listUniqueDAccounts), is(true));
  }

  @Test
  public void getPaymentsForDebtorTest() {

    String debtorAccount = "dtr-1";
    List<Payment> debtorList = implementation.getPaymentsForDebtor(debtorAccount);
    assertThat(debtorList, hasSize(2));

    debtorList.forEach(p -> assertThat(p.getDebtorAccount(), equalTo(debtorAccount)));

  }

  @Test
  public void sumByAccountsTest() {
    String debtorAccount = "dtr-1";
    String creditorAccount = "crdt-1";

    System.out.println(implementation.sumByAccounts(creditorAccount, debtorAccount));

  }

}


