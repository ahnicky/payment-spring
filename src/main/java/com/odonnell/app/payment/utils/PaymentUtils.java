package com.odonnell.app.payment.utils;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.odonnell.app.payment.pojo.Payment;

public class PaymentUtils {


  public List<Payment> parsePersonsCsv(File inputCsv) throws IOException {

    MappingIterator<Payment> iter = new CsvMapper().readerFor(Payment.class)
        .with(CsvSchema.emptySchema().withHeader()).readValues(inputCsv);

    return iter.readAll();
  }

  public boolean filterByMonth(int MonthNumber, Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.get(Calendar.MONTH) == (MonthNumber - 1);
  }
}
