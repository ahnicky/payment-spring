package com.odonnell.app.payment.pojo;

import java.math.BigDecimal;
import java.sql.Date;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"debtorAccount", "creditorAccount", "amount", "date"})
public class Payment {

  private String debtorAccount;
  private String creditorAccount;
  private BigDecimal amount;
  private Date date;
}
