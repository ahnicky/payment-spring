package com.odonnell.app.payment.beans;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;
import com.odonnell.app.payment.interfaces.PaymentRepository;
import com.odonnell.app.payment.pojo.Payment;
import com.odonnell.app.payment.utils.PaymentUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PaymentRepoImpl implements PaymentRepository {

  private List<Payment> payments = new ArrayList<>();

  PaymentUtils utils;
  private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
  private final Lock readLock = lock.readLock();
  private final Lock writeLock = lock.writeLock();


  public PaymentRepoImpl(File inputCsv, PaymentUtils utils) {
    this.utils = utils;
    initialize(inputCsv);
  }

  /**
   * Setup payment list data for other methods to consume
   */
  @Override
  public void initialize(File inputCsv) {
    try {
      writeLock.lock();
      payments = utils.parsePersonsCsv(inputCsv);
      payments.forEach(payment -> log.info(payment.toString()));
    } catch (IOException e) {
      log.error("failed to parse csv. Error: {}", e);
    } finally {
      writeLock.unlock();
    }
  }


  /**
   * return sum of amount by Creditor account id
   */
  @Override
  public BigDecimal sumByCreditorAccount(String creditorAccount) {
    try {
      readLock.lock();
      BigDecimal sum =
          payments.parallelStream().filter(Objects::nonNull).filter(p -> p.getAmount() != null)
              .filter(p -> p.getCreditorAccount().equals(creditorAccount)).map(Payment::getAmount)
              .reduce(BigDecimal.ZERO, BigDecimal::add);
      return sum;
    } finally {
      readLock.unlock();
    }
  }

  /**
   * return a sum of transactions executed between given accounts
   */
  @Override
  public BigDecimal sumByAccounts(String creditorAccount, String debtorAccount) {
    try {
      readLock.lock();
      BigDecimal sum =
          payments.parallelStream().filter(Objects::nonNull).filter(p -> p.getAmount() != null)
              .filter(p -> p.getCreditorAccount().equals(creditorAccount))
              .filter(p -> p.getDebtorAccount().equals(debtorAccount)).map(Payment::getAmount)
              .reduce(BigDecimal.ZERO, BigDecimal::add);
      return sum;
    } finally {
      readLock.unlock();
    }
  }

  /**
   * returns a total amount for a given month 1-Jan, 2-Feb...
   */
  @Override
  public BigDecimal sumByMonth(int monthNumber) {
    try {
      readLock.lock();
      BigDecimal sum = payments.parallelStream().filter(Objects::nonNull)
          .filter(p -> utils.filterByMonth(monthNumber, p.getDate())).map(Payment::getAmount)
          .reduce(BigDecimal.ZERO, BigDecimal::add);
      return sum;
    } finally {
      readLock.unlock();
    }
  }


  /**
   * return a Set of Debtor Accounts
   */
  @Override
  public List<String> listUniqueDebtorAccounts() {
    try {
      readLock.lock();
      return payments.parallelStream().map(Payment::getDebtorAccount).distinct()
          .collect(Collectors.toList());
    } finally {
      readLock.unlock();
    }
  }

  /**
   * Verify a single payment exists in collection read from csv
   */
  @Override
  public boolean contains(Payment payment) {
    try {
      readLock.lock();
      // comparing by toString value to avoid issues with comparing java.sql.date
      return payments.parallelStream()
          .anyMatch(p -> Objects.equals(payment.toString(), p.toString()));
    } finally {
      readLock.unlock();
    }
  }

  /**
   * get List of all payments for debtor id
   */
  @Override
  public List<Payment> getPaymentsForDebtor(String debtorAccount) {
    try {
      readLock.lock();
      return payments.parallelStream().filter(p -> p.getDebtorAccount().equals(debtorAccount))
          .collect(Collectors.toList());

    } finally {
      readLock.unlock();

    }
  }

}
