package com.odonnell.app.payment.config;

import java.io.File;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import com.odonnell.app.payment.beans.PaymentRepoImpl;
import com.odonnell.app.payment.interfaces.PaymentRepository;
import com.odonnell.app.payment.utils.PaymentUtils;

@Configuration
public class AppConfig {

  @Bean
  @Scope(value = "prototype")
  public PaymentRepository paymentRepo(File inputCsv) {
    return new PaymentRepoImpl(inputCsv, utils());
  }

  @Bean
  public PaymentUtils utils() {
    return new PaymentUtils();
  }
}
